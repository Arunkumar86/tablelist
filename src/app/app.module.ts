import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes} from '@angular/router';
import { AppComponent } from './app.component';
import { TableListComponent } from './table-list/table-list.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {routes} from './Routes';
import { HttpClientModule } from '@angular/common/http'; 
import {StoreModule} from '@ngrx/store';
import {appReducer} from './app.reducer';
import {appEffects} from './app-effect';
import {EffectsModule} from '@ngrx/effects'


@NgModule({
  declarations: [
    AppComponent,
   TableListComponent,
    
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,  
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    RouterModule.forRoot(routes, {useHash: true}),
    StoreModule.forRoot(appReducer),
    EffectsModule.forRoot(appEffects)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
