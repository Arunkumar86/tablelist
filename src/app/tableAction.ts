import { Action } from '@ngrx/store';


export const Get_List = 'Get_List';

export const Get_ListSuccess = 'Get_List';


export class getListAction implements Action {
  readonly type = Get_List;

  constructor(public payload: any) { }
}


export class getListActionSuccess implements Action {
    readonly type = Get_ListSuccess;
  
    constructor(public payload: any) { }
  }
  

export type Actions
  = getListAction|
  getListActionSuccess
