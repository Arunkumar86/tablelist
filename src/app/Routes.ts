import {Routes} from '@angular/router';
import {TableListComponent} from './table-list/table-list.component';


export const routes: Routes = [
  {
    path: 'tableList',
    pathMatch: 'full',
    component:  TableListComponent
  }];
 