
import * as tableAction from'./tableAction';

import {TableClass} from './app.Store'


export function tableReducer(state = new TableClass, action: tableAction.Actions) {
    switch (action.type) {
     case tableAction.Get_ListSuccess: {
       if(action.payload){
         return {...state,TableList : action.payload }
       }else{
         return {...state, TableList : []}
       }

     }
  
      default: {
        return state;
      }
    }
}