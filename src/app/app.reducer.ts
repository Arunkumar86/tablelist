import {ActionReducerMap} from '@ngrx/store'
import {Appstore} from './app.Store'
import {tableReducer} from './tableReducer'
import { TableListStore } from '../tableListStore';


export const appReducer:ActionReducerMap<Appstore> = {
    TableListSlice: tableReducer
}