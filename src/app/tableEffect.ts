import {Injectable} from '@angular/core';
import {Action, Store} from '@ngrx/store';
import {Actions, Effect, ofType} from '@ngrx/effects';
import { HttpClient } from '@angular/common/http';
import * as tableActions from './tableAction';
import {switchMap, map, catchError, exhaustMap, mergeMap} from 'rxjs/operators';

@Injectable()
export class TableListEffects {

 constructor(private actions$: Actions, private http: HttpClient){}


@Effect()
loadDashboard$ = this.actions$.pipe(
  ofType(tableActions.Get_List),
  map((action: tableActions.getListAction) => action.payload),
  exhaustMap((payload) => {
    return this.http.get("./assets/data.json")
      .pipe(
        mergeMap((data: any) => [
          new tableActions.getListActionSuccess(data)

        ]),
        catchError((error) => {
           return [( new tableActions.getListActionSuccess(error))]
        })
      )
  }));
}